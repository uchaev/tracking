import {Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {Rate} from './rate.entity';

@Injectable()
export class RateService {

    constructor(
        @InjectRepository(Rate)
        private rateRepository: Repository<Rate>,
    ) {
    }

    async create(rate, board) {
        let entity = new Rate();
        entity = {...entity, ...rate};
        entity.board = board;

        return await this.rateRepository.save(entity);
    }

    async getRatesByBoard(board) {
        return await this.rateRepository.find({where: {board}});
    }
}
