import {
    Body,
    Controller,
    Post,
    Get,
    UseGuards,
    Query,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import {RateService} from './rate.service';
import {BoardService} from '../profile/board/board.service';
import {AuthGuard} from '@nestjs/passport';

@Controller('rate')
export class RateController {
    constructor(
        private rateService: RateService,
        private boardService: BoardService,
    ) {
    }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    async get(@Query() query) {
        if (query.boardId) {
            const board = await this.boardService.getBoardById(query.boardId);
            return await this.rateService.getRatesByBoard(board);
        } else {
            return new HttpException('Bad request', HttpStatus.BAD_REQUEST) ;
        }
    }

    @Post('add')
    async add(@Body() rateDTO) {
        const board = await this.boardService.getBoardById(rateDTO.boardId);
        return await this.rateService.create(rateDTO, board);
    }

    @Get('board')
    async getBoardBySlug(@Body() rateDTO, @Query() query) {
        return await this.boardService.getBoardBySlug(query.slug);
    }
}
