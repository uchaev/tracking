import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn} from 'typeorm';
import {Board} from '../profile/board/board.entity';

@Entity()
export class Rate {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    value: string;

    @Column()
    uuid: string;

    @CreateDateColumn({type: 'timestamp'})
    createdAt: Date;

    @ManyToOne(() => Board, board => board.rates, { onDelete: 'CASCADE' })
    board: Board;
}
