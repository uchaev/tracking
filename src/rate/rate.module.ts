import {Module} from '@nestjs/common';
import {RateService} from './rate.service';
import {Rate} from './rate.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {RateController} from './rate.controller';
import {BoardService} from '../profile/board/board.service';
import {Board} from '../profile/board/board.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([Rate, Board]),
    ],
    providers: [RateService, BoardService],
    controllers: [RateController],
})
export class RateModule {
}
