import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from '../shared/user/user.entity';
import {AuthService} from './auth/auth.service';
import {AuthController} from './auth/auth.controller';
import {JwtStrategy} from './auth/jwt.strategy';
import {SharedModule} from '../shared/shared/shared.module';

@Module({
    imports: [
        TypeOrmModule.forFeature([User]), SharedModule,
    ],
    providers: [AuthService, JwtStrategy],
    controllers: [AuthController],
})
export class AuthModule {
}
