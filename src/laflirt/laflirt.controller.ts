import {
    Body,
    Controller,
    Post,
    Get,
    UseGuards,
    Delete,
    Query, Put, Param,
} from '@nestjs/common';
import {LaflirtService} from './laflirt.service';
import {AuthGuard} from '@nestjs/passport';

@Controller('laflirt')
export class LaflirtController {
    constructor(
        private laflirtService: LaflirtService,
    ) {
    }

    @Post('add')
    async add(@Body() orderDTO) {
        return await this.laflirtService.create(orderDTO);
    }

    @UseGuards(AuthGuard('jwt'))
    @Put('add')
    async update(@Body() laflirtDTO) {
        return await this.laflirtService.update(laflirtDTO);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('orders/:id')
    deleteUser(@Param() params) {
        return this.laflirtService.delete(params.id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('orders')
    async get(@Query() query) {
        if (query.id) {
            return await this.laflirtService.getById(query.id);
        } else {
            return await this.laflirtService.getAll();
        }
    }
}
