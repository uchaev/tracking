import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    OneToMany,
} from 'typeorm';
import {Intimshop} from '../intimshop/intimshop.entity';

@Entity()
export class Laflirt {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    phone: string;

    @Column()
    cart: string;

    @Column({default: ''})
    comment: string;

    @Column({default: ''})
    note: string;

    @Column({default: false})
    sync: boolean;

    @CreateDateColumn({type: 'timestamp'})
    createdAt: Date;

    @OneToMany(() => Intimshop, intimshop => intimshop.laflirt)
    intimshop: Intimshop[];
}
