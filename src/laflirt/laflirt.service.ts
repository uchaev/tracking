import {Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {Laflirt} from './laflirt.entity';

@Injectable()
export class LaflirtService {
    constructor(
        @InjectRepository(Laflirt)
        private laflirtRepository: Repository<Laflirt>,
    ) {
    }

    async create(order: any) {
        let entity = new Laflirt();
        entity = {...entity, ...order};

        return await this.laflirtRepository.save(entity);
    }

    async update(order: any) {
        let entity = new Laflirt();
        entity = {...entity, ...order};
        return await this.laflirtRepository.update(order.id, entity);
    }

    async delete(orderId) {
        return await this.laflirtRepository.delete(orderId);
    }

    async getAll() {
        return await this.laflirtRepository.find();
    }

    async getById(id: any) {
        return await this.laflirtRepository.findOne(id);
    }
}
