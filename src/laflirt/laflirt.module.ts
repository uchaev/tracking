import {Module} from '@nestjs/common';
import {LaflirtService} from './laflirt.service';
import {Laflirt} from './laflirt.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {LaflirtController} from './laflirt.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([Laflirt]),
    ],
    providers: [LaflirtService],
    controllers: [LaflirtController],
})
export class LaflirtModule {
}
