import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {User} from './user.entity';
import * as crypto from 'crypto';
import {Payload} from '../../auth/types/payload';
import {LoginDTO, RegisterDTO} from '../../auth/auth/auth.dto';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {
    }

    async findByEmail(userDTO: LoginDTO): Promise<User> {
        const {email, password} = userDTO;
        const user = await this.userRepository.findOne({email});

        if (!user) {
            throw new HttpException('invalid credentials', HttpStatus.UNAUTHORIZED);
        }

        if (crypto.createHmac('sha256', password).digest('hex') === user.password) {
            return user;
        } else {
            throw new HttpException('invalid credentials', HttpStatus.UNAUTHORIZED);
        }
    }

    async findByPayload(payload: Payload): Promise<User> {
        const {id} = payload;
        return await this.userRepository.findOne({id});
    }

    async create(user: RegisterDTO): Promise<User> {
        const checkEmail = await this.userRepository.findOne({email: user.email});
        if (checkEmail) {
            throw new HttpException('Email already registered', HttpStatus.BAD_REQUEST);
        } else {
            const entity = Object.assign(new User(), user);
            return this.userRepository.save(entity);
        }
    }
}
