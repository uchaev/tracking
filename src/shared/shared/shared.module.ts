import {Module} from '@nestjs/common';
import {UserService} from '../user/user.service';
import {User} from '../user/user.entity';
import {TypeOrmModule} from '@nestjs/typeorm';

@Module({
    imports: [
        TypeOrmModule.forFeature([User]),
    ],
    providers: [UserService],
    exports: [UserService],
})
export class SharedModule {
}
