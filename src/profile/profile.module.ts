import {Module} from '@nestjs/common';
import {BoardService} from './board/board.service';
import {Board} from './board/board.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ProfileController} from './profile.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([Board]),
    ],
    providers: [BoardService],
    controllers: [ProfileController],
})
export class ProfileModule {

}
