import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany} from 'typeorm';
import {User} from '../../shared/user/user.entity';
import {Rate} from '../../rate/rate.entity';

@Entity()
export class Board {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    desc: string;

    @Column({
        default: ''})
    slug: string;

    @ManyToOne(() => User, user => user.boards)
    user: User;

    @OneToMany(() => Rate, rate => rate.board)
    rates: Rate[];
}
