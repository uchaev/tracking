import {Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {Board} from './board.entity';
import * as crypto from 'crypto';

@Injectable()
export class BoardService {
    constructor(
        @InjectRepository(Board)
        private boardRepository: Repository<Board>,
    ) {
    }

    async create(board, user): Promise<object> {
        let entity = new Board();
        entity = {...entity, ...board};
        entity.user = user;
        entity.slug = crypto
            .createHmac('sha256', new Date().getTime().toString())
            .digest('hex')
            .slice(0, 20);
        const saveEntity = await this.boardRepository.save(entity);
        return {boardId: saveEntity.id};
    }

    async update(board, user) {
        let entity = new Board();
        entity = {...entity, ...board};
        entity.user = user;
        return await this.boardRepository.update(board.id, entity);
    }

    async getBoardBySlug(slug) {
        return await this.boardRepository.findOne({where: {slug}});
    }

    async getBoardById(id) {
        return await this.boardRepository.findOne({where: {id}});
    }

    async getBoards(user) {
        return await this.boardRepository.find({where: {user}});
    }

    async deleteBoard(board) {
        return await this.boardRepository.delete(board);
    }

}
