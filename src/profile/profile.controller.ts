import {Body, Controller, Post, Get, UseGuards, Param, Query, Delete, Put} from '@nestjs/common';
import {User} from '../utilities/user.decorator';
import {BoardService} from './board/board.service';
import {AuthGuard} from '@nestjs/passport';

@Controller('profile')
@UseGuards(AuthGuard('jwt'))
export class ProfileController {
    constructor(
        private boardService: BoardService,
    ) {
    }

    @Post('boards')
    async create(@Body() boardDTO, @User() user) {
        return await this.boardService.create(boardDTO, user);
    }

    @Put('boards')
    async update(@Body() boardDTO, @User() user) {
        return await this.boardService.update(boardDTO, user);
    }

    @Get('boards')
    async getBoards(@User() user, @Param() param, @Query() query) {

        if (query.id) {
            return await this.boardService.getBoardById(query.id);
        } else {
            return await this.boardService.getBoards(user);
        }
    }

    @Delete('boards/:id')
    deleteUser(@Param() params) {
        return this.boardService.deleteBoard(params.id);
    }

}
