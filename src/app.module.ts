import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {AuthModule} from './auth/auth.module';
import {ProfileModule} from './profile/profile.module';
import {SharedModule} from './shared/shared/shared.module';
import {RateModule} from './rate/rate.module';
import {LaflirtModule} from './laflirt/laflirt.module';
import {IntimshopModule} from './intimshop/intimshop.module';

@Module({
    imports: [TypeOrmModule.forRoot({autoLoadEntities: true}), SharedModule, AuthModule, ProfileModule, RateModule, LaflirtModule, IntimshopModule],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
