import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, OneToMany} from 'typeorm';
import {Laflirt} from '../laflirt/laflirt.entity';

@Entity()
export class Intimshop {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    response: string;

    @Column({default: ''})
    comment: string;

    @CreateDateColumn({type: 'timestamp'})
    createdAt: Date;

    @ManyToOne(() => Laflirt, laflirt => laflirt.intimshop, {onDelete: 'CASCADE'})
    laflirt: Laflirt;
}
