import {
    Body,
    Controller,
    Post,
    Get,
    UseGuards,
    Query,
    HttpException,
    HttpStatus, Put,
} from '@nestjs/common';
import {IntimshopService} from './intimshop.service';
import {LaflirtService} from '../laflirt/laflirt.service';
import {AuthGuard} from '@nestjs/passport';
import Axios from 'axios';
import {User} from '../utilities/user.decorator';

const API = 'https://www.intimshop.ru';

@UseGuards(AuthGuard('jwt'))
@Controller('intimshop')
export class IntimshopController {
    constructor(
        private intimshopService: IntimshopService,
        private laflirtService: LaflirtService,
    ) {
    }

    @Post('add')
    async add(@Body() orderDTO: any) {
        try {
            const {data} = await Axios.post(`${API}/orders/drafts/add/`, {
                items: orderDTO.items,
                phone: orderDTO.phone,
                comment: orderDTO.comment,
                partner: 19151,
            });

            console.log(JSON.stringify(data), 'from intimShop');

            const laflirt = await this.laflirtService.getById(orderDTO.id);
            return this.intimshopService.create({response: JSON.stringify(data)}, laflirt);
        } catch (e) {
            throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
        }
    }

    @Get('orders')
    async get(@Query() query) {
        if (query.id) {
            return await this.intimshopService.getById(query.id);
        } else if (query.laflirtId) {
            const laflirt = await this.laflirtService.getById(query.laflirtId);
            return await this.intimshopService.getByLaflirt(laflirt);
        } else {
            return await this.intimshopService.getAll();
        }
    }
}
