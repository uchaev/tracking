import {Injectable} from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {Intimshop} from './intimshop.entity';

@Injectable()
export class IntimshopService {
    constructor(
        @InjectRepository(Intimshop)
        private intimshopRepository: Repository<Intimshop>,
    ) {
    }

    async create(intimshop, laflirt) {
        let entity = new Intimshop();
        entity = {...entity, ...intimshop};
        entity.laflirt = laflirt;

        return await this.intimshopRepository.save(entity);
    }

    async getAll() {
        return await this.intimshopRepository.find();
    }

    async getByLaflirt(laflirt) {
        return await this.intimshopRepository.find({where: {laflirt}});
    }

    async getById(id: any) {
        return await this.intimshopRepository.findOne(id);
    }
}
