import {Module} from '@nestjs/common';
import {IntimshopService} from './intimshop.service';
import {Intimshop} from './intimshop.entity';
import {Laflirt} from '../laflirt/laflirt.entity';
import {TypeOrmModule} from '@nestjs/typeorm';
import {IntimshopController} from './intimshop.controller';
import {LaflirtService} from '../laflirt/laflirt.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Intimshop, Laflirt]),
    ],
    providers: [IntimshopService, LaflirtService],
    controllers: [IntimshopController],
})
export class IntimshopModule {
}
